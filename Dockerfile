# Use an official WordPress image as the base image
FROM wordpress:latest

# Set environment variables for the database connection
ENV WORDPRESS_DB_HOST=stage-wordpress-rds.cg0yjt5zptke.eu-west-1.rds.amazonaws.com \
    WORDPRESS_DB_USER=admin \
    WORDPRESS_DB_PASSWORD=varrow1234 \
    WORDPRESS_DB_NAME=varrowdb

# Copy your WordPress files into the container
COPY . /var/www/html/
